package com.nbodev.https;

import java.time.LocalDateTime;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/test")
public class MyController {

	@GetMapping
	public String getDemo() {
		return LocalDateTime.now().toString();
	}
}
