### Introduction

This article describes how to use Spring Boot with a self signed certificate.

Then with we will deploy the Spring Boot application on AWS and issue a Let’s encrypt certificate against a dynamic DNS service Duck DNS.

The full article is here: [Spring Boot / Duck DNS / Let’s encrypt / AWS](https://nbodev.medium.com/spring-boot-duck-dns-lets-encrypt-aws-af469b52308a)

### Warning

- Make sure tom use a JDK 11 > 11.0.2, else you will get this bug https://bugs.java.com/bugdatabase/view_bug.do?bug_id=JDK-8214513 


### Generate the self signed certificate

- Run the following:

```
keytool -genkeypair -alias springbootselfsigned -keyalg RSA -keysize 2048 -keystore springbootselfsigned.p12 -storetype PKCS12 -validity 3650
```

- Copy the generated file `springbootselfsigned.p12` in your classpath `src/main/resources` and set the right configuration in your `application.yml`:

```
# Spring Boot server port
server:
  port: 8443
  ssl:
    key-store-type: PKCS12 # The format used for the keystore. It could be set to JKS in case it is a JKS file
    key-store: classpath:springbootselfsigned.p12 # The path to the keystore containing the certificate
    key-store-password: password1234 # The password used to generate the certificate
    key-alias: springbootselfsigned # The alias mapped to the certificate
security:
  require-ssl: true
```

- Visit this url, the certificate is not valid but that's why you need to accept the browser warning:

```
https://localhost:8443/test 
```

### Generate the Let's encrypt certificate

- Provision an EC2 instance on AWS and a domain name on duckdns.org.
- Update IP address for your domain on duckdns.org, it should target the Public IPv4 address of your EC2.
- Open the port 80 on your EC2, else the generation of the certificate will fail.
- Install certbot on the EC2 instance, below the ubuntu commands:

```
sudo apt-get update
sudo apt-get install certbot
```

- Generate the certificate for our subdomain yourdomain.duckdns.org:

```
sudo certbot certonly --standalone -d yourdomain.duckdns.org
```

- At this stage we need to produce a PKCS12 certificate as we did with the self signed certificate, in order to produce such file we run:

```
sudo openssl pkcs12 -export -in /etc/letsencrypt/live/yourdomain.duckdns.org/fullchain.pem -inkey /etc/letsencrypt/live/yourdomain.duckdns.org/privkey.pem -out springbootletsencrypt.p12 -name springbootletsencrypt -CAfile /etc/letsencrypt/live/yourdomain.duckdns.org/chain.pem -caname root
```

- Make sure you change the owner of this file as it belongs to the root user:

```
sudo chown ubuntu:ubuntu  springbootletsencrypt.p12
```

- Update the application.yml and use it as an external file:

```
# Spring Boot server port
server:
  port: 8443
  ssl:
    key-store-type: PKCS12 # The format used for the keystore. It could be set to JKS in case it is a JKS file
    key-store: /home/ubuntu/springbootletsencrypt.p12 # The path to the keystore containing the certificate
    key-store-password: password1234 # The password used to generate the certificate
    key-alias: springbootletsencrypt # The alias mapped to the certificate
security:
  require-ssl: true
```

- Run: 

```
java -Dspring.config.location=file:/home/ubuntu/application.yml -jar springboot-https-0.0.1-SNAPSHOT.jar
```

- Visit this url, no more browser warning:

```
https://yourdomain.duckdns.org:8443/test
```

